# Intermediate-Python

"This course begins with a review of basic Python concepts. How much time we spend on that depends on the
backgrounds of the students and the questions that arise during the review. Given that most of you have a
basic understanding of Python and tend to use Python in a scripting/DevOps background, I want to spend a
little extra time on that first section to be sure we all have a common foundation."

The Multithreading/Multiprocessing unit should be updated with more modern ways
of doing concurrency in Python.

